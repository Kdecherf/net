# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=greenbone project=gsa tag=v${PV} ] \
    cmake [ api=2 ] \
    python [ blacklist=3 multibuild=false ] \
    systemd-service

SUMMARY="OpenVAS Manager"
HOMEPAGE+=" http://www.openvas.org"

LICENCES="BSD-3 GPL-2 LGPL-2 MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/polib[python_abis:*(-)?]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32]
        dev-libs/gnutls[>=3.2.15]
        dev-libs/libgcrypt
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        net-analyzer/openvas-libraries[>=9.0.0]
        net-libs/libmicrohttpd[>=0.9.0][ssl]
    run:
        net-analyzer/openvas-manager[>=7.0.1]
        net-analyzer/openvas-scanner[>=5.1.1]
    test:
        dev-util/cppcheck
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-7.0.3-fix-po.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DDATADIR:PATH=/usr/share
    -DGSAD_PID_DIR:PATH=/run
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DLOCALSTATEDIR:PATH=/var
    -DOPENVAS_RUN_DIR:PATH=/run
    -DPYTHON_EXECUTABLE:PATH=${PYTHON}
    -DSBINDIR:PATH=/usr/$(exhost --target)/bin
    -DSYSCONFDIR:PATH=/etc
)

src_install() {
    cmake_src_install

    install_systemd_files

    insinto /etc/conf.d
    hereins gsad.conf << EOF
#GSAD_LISTEN_ADDRESS=--listen=127.0.0.1
#GSAD_ALLOW_HEADER_HOST=--allow-header-host=host.domain.example
#GSAD_SSL_CERT=--ssl-certificate=/etc/ssl/openvas.crt
#GSAD_SSL_KEY=--ssl-private-key=/etc/ssl/openvas.key
EOF
}

