# Copyright 2011 Dimitry Ishenko <dimitry.ishenko@gmail.com>
# Copyright 2012-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=SABnzbd-${PV}
require github [ user=sabnzbd project=sabnzbd pnv=${MY_PNV}-src suffix=tar.gz release=${PV} ]
require python [ blacklist=3 python_opts='[sqlite]' ]

SUMMARY="SABnzbd is an open-source cross-platform binary newsreader"
DESCRIPTION="
SABnzbd is an open-source cross-platform binary newsreader.
It simplifies the process of downloading from Usenet dramatically,
thanks to its friendly web-based user interface and advanced
built-in post-processing options that automatically verify, repair,
extract and clean up posts downloaded from Usenet.
"
HOMEPAGE+=" https://sabnzbd.org"
BUGS_TO="philantrop@exherbo.org"
LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    run:
        app-arch/p7zip[>=9.20]
        app-arch/par2cmdline[>=0.4]
        app-arch/unrar[>5.00]
        app-arch/unzip[>=6.00]
        dev-python/Cheetah[>2.0.1][python_abis:*(-)?]
        dev-python/cryptography[>=1.0]
        dev-python/sabyenc[>=3.3.2]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1.0.0] )
    suggestion:
        run:
            dev-python/dbus-python [[
                description = [ Enable option to Shutdown/Restart/Standby PC on queue finish ]
            ]]
"

WORK=${WORKBASE}/${MY_PNV}

src_install() {
    local DESTDIR=/usr/share/${PN}

    dodir ${DESTDIR}
    insinto ${DESTDIR}
    doins -r *
    edo chmod 0755 "${IMAGE}"/${DESTDIR}/SABnzbd.py

    herebin sabnzbd+ <<EOF
#!/bin/bash
${DESTDIR}/SABnzbd.py "\$@"
EOF

    emagicdocs
}

