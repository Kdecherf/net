# Copyright 2009 Nestor Ovroy <novroy@riseup.net>
# Copyright 2012 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require googlecode [ suffix=tar.gz ]

SUMMARY="Use socks-friendly applications with Tor"
DESCRIPTION="
Torsocks allows you to use most socks-friendly applications in a safe way with
Tor. It ensures that DNS requests are handled safely and explicitly rejects UDP
traffic from the application you're using.
"
BUGS_TO="philantrop@exherbo.org"
LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""
DEPENDENCIES=""

# See comment in src_test below.
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/fix-symbol-notfound.patch )
# Upstream obviously thinks datadir equals docdir...
DEFAULT_SRC_CONFIGURE_PARAMS=( --datadir=/usr/share/doc/${PNVR} )

src_test() {
    pushd test

    # The script expects torsocks to be installed already. It could probably be
    # fixed and I started that but ultimately didn't care enough to do it myself.
    # The tests do assume a lot of things as well so that'll have to be fixed as
    # as well. You'll have to deal with a lot of sandbox violations, too.
    # Furthermore, the tests get installed without some critical components so
    # they can only be run from the unpacked sources.
    # And did I mention that a test failure isn't fatal?
    ./run_tests.sh

    popd
}

src_install() {
    default

    # Don't install patches that were already applied by upstream.
    edo rm "${IMAGE}"/usr/share/doc/${PNVR}/*.patch

    # Don't install half of the tests either because they're simply broken then.
    edo rm "${IMAGE}"/usr/share/doc/${PNVR}/{run_tests.sh,expectedresults.txt}
}

