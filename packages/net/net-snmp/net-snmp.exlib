# Copyright 2012 Kevin Decherf <kevin@kdecherf.com>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

export_exlib_phases src_install src_test pkg_postinst

SUMMARY="Net-SNMP is a suite of applications used to implement SNMP v1, SNMP v2c and SNMP v3 usingboth IPv4 and IPv6"
HOMEPAGE="http://www.net-snmp.org"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.gz"

REMOTE_IDS="freecode:${PN}"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# two com2sec6 directive tests fail
RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-util/elfutils
        net-libs/libnl:3.0
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        sys-apps/pciutils
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --enable-local-smux
    --enable-mfd-rewrites
    --disable-embedded-perl
    --disable-static
    --with-default-snmp-version="3"
    --with-elf
    --with-logfile="/var/log/net-snmpd.log"
    --with-nl
    --with-openssl
    --with-persistent-directory="/var/lib/net-snmp"
    --with-sys-contact="root@localhost"
    --with-sys-location="Unknown"
    --with-temp-file-pattern=/run/net-snmp/snmp-tmp-XXXXXX
    --without-libwrap
    --without-perl-modules
    --without-python-modules
    --without-rpm
    --without-mysql
)

net-snmp_src_install() {
    default

    keepdir /etc/snmp
    keepdir /var/lib/net-snmp

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/net-snmp 0755 root root
EOF

}

net-snmp_src_test() {
    esandbox allow_net --connect "unix:/tmp/snmp-test-T115agentxperl_simple-*/agentx_socket"
    esandbox allow_net "unix:/tmp/snmp-test-T115agentxperl_simple-*/agentx_socket"
    default
    esandbox disallow_net "unix:/tmp/snmp-test-T115agentxperl_simple-*/agentx_socket"
    esandbox disallow_net --connect "unix:/tmp/snmp-test-T115agentxperl_simple-*/agentx_socket"
}

net-snmp_pkg_postinst() {
    elog "To make your first configuration file, use 'snmpconf -g basic_setup' and move the"
    elog "generated snmpd.conf to the /etc/snmp/ configuration directory."
}

