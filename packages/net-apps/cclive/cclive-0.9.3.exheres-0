# Copyright 2009, 2012 Ingmar Vanhassel
# Copyright 2011 Sterling X. Winter <replica@exherbo.org>
# Copyright 2012,2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.xz ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Command line video download tool"
DESCRIPTION="
cclive is a command line tool for downloading videos from YouTube and other similar video websites
that require Adobe Flash to view the video content. It has low memory footprint compared to other
similar tools.

Features:
 * Retry interrupted downloads automatically
 * quvi integration, supports 20+ websites
 * Flexible output filename options
 * Can go to background

Additional tools:
 * grake - YouTube link scanner
 * gcap - YouTube closed captions downloader
 * umph - YouTube feed parser for playlists, etc.
"

BUGS_TO="ingmar@exherbo.org"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc
        dev-util/pkg-config
    build+run:
        dev-libs/boost[>=1.49.0]
        dev-libs/libquvi[>=0.9]
        dev-libs/pcre[>=8.02]
        gnome-bindings/glibmm:2.4[>=2.24]
        net-misc/curl[>=7.18.0]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.9.3-boost.patch
    "${FILES}"/${PN}-0.9.3-gcc5.patch
    "${FILES}"/${PN}-0.9.3-fix-rpath.patch
    "${FILES}"/${PN}-0.9.3-boost-1.67.patch
)

